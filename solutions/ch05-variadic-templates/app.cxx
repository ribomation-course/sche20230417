#include <iostream>
#include <stdexcept>
using std::cout;

template<typename... Args>
auto multiply(Args... args) { //multiply(int a, int b, int c, int d)
    if constexpr (sizeof...(args) == 0) {
        throw std::invalid_argument("multiply() requires at least one argument");
    }
    return (1 * ... * args); //multiply(2, 3, 4, 5) --> return (1 * 2 * 3 * 4 * 5)
}

int main() {
    cout << "mult(1..5) = " << multiply(2, 3, 4, 5) << "\n";
    cout << "mult(5) = " << multiply(5) << "\n";
    cout << "mult(-5..(0)...+5) = " << multiply(-5, -4, -3, -2, -1, 1, 2, 3, 4, 5) << "\n";
    try { multiply(); }
    catch (std::invalid_argument const& e) { cout  << e.what() << "\n"; }
}
