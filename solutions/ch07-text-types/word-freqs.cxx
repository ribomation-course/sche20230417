#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <string_view>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <vector>
#include <chrono>
#include <tuple>
#include <stdexcept>
#include <memory>
#include <cmath>

using std::cout;
using std::string;
using std::string_view;
using std::pair;
using std::unique_ptr;
using namespace std::literals;
namespace chr = std::chrono;

using WordFreqs   = std::unordered_map<string_view, unsigned>;
using SortedFreqs = std::vector<pair<string_view, unsigned>>;

auto load(const string& filename) -> std::tuple<unique_ptr<char*>, string_view>;
auto aggregate(string_view payload, unsigned min = 4) -> WordFreqs;
auto sort(WordFreqs const& wf, unsigned maxWords) -> SortedFreqs;
auto operator <<(std::ostream& os, SortedFreqs const& wf) -> std::ostream&;

int main() {
    auto startTime   = chr::high_resolution_clock::now();
    auto filename    = "../musketeers.txt"s;
    auto minWordSize = 4U;
    auto maxWords    = 25U;

    auto [resource, payload] = load(filename);
    cout << "Loaded " << payload.size() << " chars\n";

    auto allWords = aggregate(payload, minWordSize);
    auto mostFreq = sort(allWords, maxWords);
    cout << mostFreq << "\n";

    auto endTime     = chr::high_resolution_clock::now();
    auto elapsedTime = chr::duration_cast<chr::milliseconds>(endTime - startTime);
    cout << "Elapsed " << elapsedTime.count() << " ms\n";
}

auto load(const string& filename) -> std::tuple<unique_ptr<char*>, string_view> {
    auto f = std::ifstream{filename};
    if (!f) throw std::invalid_argument{"cannot open "s + filename};

    size_t size = f.seekg(0, std::ios_base::end).tellg();
    f.seekg(0);

    auto buf = new char[size + 1];
    f.read(buf, static_cast<std::streamsize>(size));
    buf[size] = '\0';
    return make_tuple(std::make_unique<char*>(buf), string_view{buf, size});
}

auto aggregate(string_view payload, unsigned int min) -> WordFreqs {
    auto       f       = WordFreqs{};
    auto const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"sv;

    auto start = 0UL;
    do {
        start     = payload.find_first_of(letters, start);
        if (start == string_view::npos) break;

        auto end  = payload.find_first_not_of(letters, start);
        if (end == string_view::npos) break;

        auto word = payload.substr(start, end - start);
        start = end + 1;

        if (word.size() >= min) ++f[word];
    } while (start < payload.size());

    return f;
}

auto sort(const WordFreqs& wf, unsigned int maxWords) -> SortedFreqs {
    auto result = SortedFreqs{wf.begin(), wf.end()};
    std::partial_sort(result.begin(), result.begin() + maxWords,
                      result.end(),
                      [](auto const& lhs, auto const& rhs) { return lhs.second > rhs.second; });
//    std::sort(result.begin(), result.end(),
//                      [](auto const& lhs, auto const& rhs) { return lhs.second > rhs.second; });
    result.erase(result.begin() + maxWords, result.end());
    result.shrink_to_fit();
    return result;
}

auto operator<<(std::ostream& os, SortedFreqs const& wf) -> std::ostream& {
    for (auto&& [word, freq]: wf)
        os << std::setw(12) << word << ": " << freq << "\n";
    return os;
}
