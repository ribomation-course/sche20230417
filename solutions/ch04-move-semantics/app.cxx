#include <iostream>
#include <cstring>

using std::cout;

class Person {
    char* name;
public:
    Person() : name(nullptr) {}
    explicit Person(char const* name_) : name(::strdup(name_)) {}
    ~Person() { delete[] name; }

    Person(const Person&) = delete;
    Person(Person&& rhs) noexcept {
        name = rhs.name;
        rhs.name = nullptr;
    }

    friend auto operator<<(std::ostream& os, Person const& p) -> std::ostream& {
        return os << "Person{" << (p.name ? p.name : "??") << "} @ " << &p;
    }

    void reverse() {
        if (name) {
            auto len = ::strlen(name);
            for (auto i = 0; i < len / 2; ++i) {
                std::swap(name[i], name[len - i - 1]);
            }
        }
    }
};

Person func(Person q) {
    q.reverse();
    return q;
}

int main() {
    auto nisse = Person{"Nisse Hult"};
    cout << "nisse: " << nisse << "\n";
    auto nisse2 = func(std::move(nisse));
    cout << "nisse2: " << nisse2 << "\n";
    cout << "nisse: " << nisse << "\n";
}
