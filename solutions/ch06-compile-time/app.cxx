#include <iostream>
#include <string>
#include <type_traits>

using std::cout;
using namespace std::string_literals;

template<typename T>
constexpr T factorial(T n) {
    static_assert(std::is_integral_v<T>, "T must be of integer type");
    if (n <= 0) {
        throw std::invalid_argument{"argument '"s + std::to_string(n) + "' must be positive"s};
        //return T{};
    }
    T result = 1;
    for (; n > 0; --n) result *= n;
    return result;
}

int main() {
    cout << "5! = " << factorial(5) << "\n";
    try {factorial(0);} catch (std::logic_error const& e) {cout << e.what() << "\n";}
    try {factorial(-5);} catch (std::logic_error const& e) {cout << e.what() << "\n";}
     long values[] = {
            factorial(1L),
            factorial(10L),
            factorial(20L),
            factorial(30L),
            factorial(40L),
            factorial(41L),
            factorial(70L),
    };
    for (auto&& value: values) cout << value << "\n";
}
