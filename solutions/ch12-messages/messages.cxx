#include <iostream>
#include <iomanip>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <syncstream>

using std::cout;
using std::mutex;
using std::condition_variable;
using std::queue;
using std::unique_lock;


template<typename T>
class MessageQueue {
    mutex lck;
    condition_variable notEmpty;
    condition_variable notFull;
    queue<T> inbox;
    unsigned const capacity;

    [[nodiscard]] bool empty() const { return inbox.empty(); }
    [[nodiscard]] bool full()  const { return inbox.size() == capacity; }

public:
    explicit MessageQueue(unsigned max = 16) : capacity{max} {}
    MessageQueue(const MessageQueue<T>&) = delete;
    MessageQueue<T>& operator=(const MessageQueue<T>&) = delete;

    void put(T x) {
        auto g = unique_lock<mutex>{lck};
        notFull.wait(g, [this] { return !full(); });
        inbox.push(x);
        notEmpty.notify_all();
    }

    T get() {
        auto g = unique_lock<mutex>{lck};
        notEmpty.wait(g, [this] { return !empty(); });
        T x = inbox.front();
        inbox.pop();
        notFull.notify_all();
        return x;
    }
};

struct Producer {
    unsigned const max;
    MessageQueue<unsigned>& out;
    Producer(unsigned max, MessageQueue<unsigned>& out) : max(max), out(out) {}

    void operator()() {
        for (auto k = 1U; k <= max; ++k) out.put(k);
        out.put(0);
    }
};

struct Consumer {
    MessageQueue<unsigned>& in;
    explicit Consumer(MessageQueue<unsigned>& in) : in(in) {}

    void operator()() {
        for (auto msg = in.get(); msg != 0; msg = in.get())
            std::osyncstream(cout) << "C: " << std::setw(6) << msg << "\n";
    }
};

int main(int argc, char** argv) {
    auto const N = argc==1 ? 1'000U : std::stoi(argv[1]);
    {
        auto Q    = MessageQueue<unsigned>{};
        auto cons = std::jthread{Consumer{Q}};
        auto prod = std::jthread{Producer{N, Q}};
    }
    cout<<"[main] done\n";
}
