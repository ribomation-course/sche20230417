cmake_minimum_required(VERSION 3.25)
project(ch10_files)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(file-counts file-counts.cxx)
target_compile_options(file-counts PRIVATE ${WARN})
