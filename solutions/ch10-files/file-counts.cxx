#include <iostream>
#include <fstream>
#include <filesystem>
#include <unordered_set>
#include <tuple>
#include <chrono>

namespace fs = std::filesystem;
namespace cr = std::chrono;
using std::cout;
using std::string;

auto count(const fs::directory_entry& entry) -> std::tuple<string, unsigned>;
bool is_text_file(const fs::directory_entry& e);

int main() {
    auto dir = fs::path{"../../"};
    if (!fs::is_directory(dir)) {
        std::cout << "Not a directory: " << dir << std::endl;
        return 1;
    }
    cout << "Directory: " << fs::canonical(dir) << "\n";

    auto startTime = cr::high_resolution_clock::now();
    auto totalLineCount = 0U;
    for (auto& e: fs::recursive_directory_iterator(dir)) {
        if (is_text_file(e)) {
            auto [filename, lineCount] = count(e);
            cout << filename << ": " << lineCount << "\n";
            totalLineCount += lineCount;
        }
    }
    cout << "Total: " << totalLineCount << " lines of code\n";

    auto endTime = cr::high_resolution_clock::now();
    auto elapsed = cr::duration_cast<cr::milliseconds>(endTime - startTime);
    cout << "Elapsed: " << elapsed.count() << " ms\n";
}

bool is_text_file(const fs::directory_entry& e) {
    static const auto EXTS = std::unordered_set<std::string>{
            ".h", ".hpp", ".hxx", ".c", ".cpp", ".cxx", ".cc",
            ".hh", ".txt", ".md", ".json", ".js", ".css", ".html", ".xml"
    };
    return e.is_regular_file() && EXTS.contains(e.path().extension().string());
}

auto count(const fs::directory_entry& entry) -> std::tuple<std::string, unsigned int> {
    auto file = std::ifstream{entry.path()};
    auto lineCount = 0U;
    for (auto line = std::string{}; std::getline(file, line);) ++lineCount;
    return {entry.path().filename().string(), lineCount};
}
