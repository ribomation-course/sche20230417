#include <iostream>
#include <string>
#include <thread>
#include <syncstream>
#include <chrono>
#include <vector>

using namespace std::chrono_literals;
using std::cout;

int main() {
    auto const T = std::jthread::hardware_concurrency();
    auto const N = 100U;
    {
        auto body = [](unsigned id) {
            auto tab = std::string((id - 1) * 3, ' ');
            for (auto k = 0U; k < N; ++k) {
                std::this_thread::sleep_for(50ms);
                std::osyncstream(cout) << tab << "[" << id << "] " << k << "\n";
            }
            std::osyncstream(cout) << tab << "[" << id << "] DONE\n";
        };

        auto threads = std::vector<std::jthread>{};
        threads.reserve(T);
        for (auto id = 1U; id <= T; ++id) threads.emplace_back(body, id);
    }
    cout << "[main] exit\n";
}
