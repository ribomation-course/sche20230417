cmake_minimum_required(VERSION 3.25)
project(ch09_stl)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(app app.cxx)
target_compile_options(app PRIVATE ${WARN})
