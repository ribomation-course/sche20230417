#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>
#include <unordered_set>
#include <algorithm>
#include <iterator>
#include <cctype>

using namespace std::string_literals;
using std::cout;


int main() {
    auto filename = "../app.cxx"s;
    auto f = std::ifstream{filename};
    if (!f) throw std::runtime_error("Could not open file: "s + filename);

    auto words = std::unordered_multiset<std::string>{};
    for (std::string word; f >> word;) {
        std::string w{};
        std::copy_if(word.begin(), word.end(), std::back_inserter(w), [](char ch){
            return ::isalpha(ch);
        });
        if (!w.empty()) words.insert(w);
    }
    for (auto it = words.begin(); it != words.end();) {
        auto cnt = words.count(*it);
        cout << *it << ": " << cnt << "\n";
        std::advance(it, cnt);
    }
}
