#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::string;
using std::vector;
using namespace std::string_literals;

int main() {
    {
        vector<string> words = {"C++"s, "is"s, "a"s, "cool"s, "language"s};
        for (string const& w: words) cout << w << " ";
        cout << "\n";
    }
    {
        string sentence = "C++ is indeed a very cool langauge!"s;
        if (auto idx = sentence.find("cool"s); idx != string::npos) {
            cout << "(1) found: " << sentence.substr(idx) << "\n";
        } else {
            cout << "(1) Not found idx=" << idx << "\n";
        }
        if (auto idx = sentence.find("COOL"s); idx != string::npos) {
            cout << "(2) found: " << sentence.substr(idx) << "\n";
        } else {
            cout << "(2) Not found idx=" << idx << "\n";
        }
    }
}
