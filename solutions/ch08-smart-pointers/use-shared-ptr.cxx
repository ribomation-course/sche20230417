#include <iostream>
#include <memory>
#include "person.hxx"
#include "use-shared-ptr.hxx"

using std::cout;
using std::endl;
using ribomation::Person;
using namespace std::string_literals;

auto func2(std::shared_ptr<Person> q) -> std::shared_ptr<Person> {
    q->incrAge();
    cout << "[func2] q: " << *q << " [" << q.use_count() << "]" << endl;
    return q;
}

auto func1(std::shared_ptr<Person> q) -> std::shared_ptr<Person> {
    q->incrAge();
    cout << "[func1] q: " << *q << " [" << q.use_count() << "]" << endl;
    return func2(q);
}

void use_shared_ptr() {
    auto justin = std::make_shared<Person>("Justin Time"s, 37);
    cout << "[use_shared_ptr] justin: " << *justin << " [" << justin.use_count() << "]" << endl;
    {
        auto p = func1(justin);
        cout << "[use_shared_ptr] p: " << *p << " [" << p.use_count() << "]" << endl;
    }
    cout << "[use_shared_ptr] justin: " << *justin << " [" << justin.use_count() << "]" << endl;
}

