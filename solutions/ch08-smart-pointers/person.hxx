#pragma once

#include <iostream>
#include <string>
#include <utility>

namespace ribomation {
    using std::string;
    using std::cout;
    using std::endl;

    class Person {
        const string name;
        unsigned age;
    public:
        Person(string name_, unsigned int age_) : name{std::move(name_)}, age{age_} {
            cout << "+Person{" << name << ", " << age << "} @ " << this << endl;
        }
        ~Person() {
            cout << "~Person() @ " << this << endl;
        }

        Person(const Person&) = delete;
        auto operator=(const Person&) -> Person& = delete;

        unsigned incrAge() {
            ++age;
            return age;
        }

        friend auto operator<<(std::ostream& os, Person const& p) -> std::ostream& {
            return os << "Person{" << p.name << ", " << p.age << "} @ " << &p;
        }
    };

}

