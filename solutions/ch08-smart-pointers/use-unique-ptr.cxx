#include <iostream>
#include <memory>
#include "person.hxx"
#include "use-unique-ptr.hxx"

using std::cout;
using std::endl;
using ribomation::Person;
using namespace std::string_literals;

auto func2(std::unique_ptr<Person> q) -> std::unique_ptr<Person> {
    q->incrAge();
    cout << "[func2] q: " << *q << endl;
    return q;
}

auto func1(std::unique_ptr<Person> q) -> std::unique_ptr<Person> {
    q->incrAge();
    cout << "[func1] q: " << *q << endl;
    return func2(std::move(q));
}

void use_unique_ptr() {
    auto anna = std::make_unique<Person>("Anna Conda"s, 42);
    cout << "[use_unique_ptr] anna: " << *anna << endl;

    auto p = func1(std::move(anna));

    cout << "[use_unique_ptr] p: " << *p << endl;
    cout << "[use_unique_ptr] anna: " << anna.get() << endl;
}

