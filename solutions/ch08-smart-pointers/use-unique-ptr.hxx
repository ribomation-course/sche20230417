#pragma once
#include <memory>
#include "person.hxx"

void use_unique_ptr();
auto func2(std::unique_ptr<ribomation::Person> q) -> std::unique_ptr<ribomation::Person>;
auto func1(std::unique_ptr<ribomation::Person> q) -> std::unique_ptr<ribomation::Person>;

