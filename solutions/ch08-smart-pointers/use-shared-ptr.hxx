#pragma once
#include <memory>
#include "person.hxx"

void use_shared_ptr();
auto func2(std::shared_ptr<ribomation::Person> q) -> std::shared_ptr<ribomation::Person>;
auto func1(std::shared_ptr<ribomation::Person> q) -> std::shared_ptr<ribomation::Person>;

