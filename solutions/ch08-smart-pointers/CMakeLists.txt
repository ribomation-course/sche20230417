cmake_minimum_required(VERSION 3.25)
project(ch08_smart_pointers)

set(CMAKE_CXX_STANDARD 20)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(app
        person.hxx
        use-unique-ptr.hxx
        use-unique-ptr.cxx
        use-shared-ptr.hxx
        use-shared-ptr.cxx
        app.cxx
        )
target_compile_options(app PRIVATE ${WARN})
