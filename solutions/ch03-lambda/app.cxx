#include <iostream>
#include <algorithm>
#include <functional>
using std::cout;

auto count_if(int* first, int const* last, std::function<bool(int)> p) {
    auto cnt = 0U;
    for (; first != last; ++first) if (p(*first)) ++cnt;
    return cnt;
}

int main() {
    int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    auto const N = sizeof(numbers) / sizeof(numbers[0]);
    auto print = [](auto e) { cout << e << " "; };
    std::for_each(numbers, numbers + N, print);
    cout << "\n";

    auto factor = 42;
    std::transform(numbers, numbers + N, numbers, [factor](auto e) { return factor * e; });
    std::for_each(numbers, numbers + N, print);
    cout << "\n";

    auto result = count_if(numbers, numbers + N, [factor](int e) { return (e/factor) % 3 == 0; });
    cout << "result: " << result << "\n";
}
