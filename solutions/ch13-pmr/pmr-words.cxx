#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <set>
#include <unordered_map>
#include <memory_resource>
#include <cstdint>
#include <cctype>

using std::pmr::string;
using std::pmr::set;

using std::pmr::null_memory_resource;
using std::pmr::monotonic_buffer_resource;
using std::pmr::unsynchronized_pool_resource;
using std::pmr::set_default_resource;

auto strip(string s) -> string;

void load(std::istream& file);

std::uint8_t storage[100'000]{};
auto buffer = monotonic_buffer_resource{std::data(storage), std::size(storage), null_memory_resource()};
auto memory = unsynchronized_pool_resource{&buffer};
auto repo   = std::unordered_map<void*, size_t>{};

auto operator new[](size_t nbytes) -> void* {
    auto block = memory.allocate(nbytes);
    std::cerr << "operator new[](" << nbytes << ") -> " << block << "\n";
    repo[block] = nbytes;
    return block;
}

void operator delete[](void* ptr) noexcept {
    auto block = repo.find(ptr);
    if (block != std::end(repo)) {
        std::cerr << "operator delete[](" << block->first << ", " << block->second << ")\n";
        memory.deallocate(ptr, block->second);
        repo.erase(block);
    }
}

int main() {
    set_default_resource(&memory);
    {
        std::cerr << "std::ifstream is not using PMR\n";
        auto file = std::ifstream{"../pmr-words.cxx"};
        if (!file) {
            std::cerr << "Could not open file\n";
            return 1;
        }
        load(file);
    }
}

void load(std::istream& file) {
    auto words = set<string>{};
    for (string word; file >> word;) {
        word = strip(std::move(word));
        if (word.empty()) continue;
        words.insert(std::move(word));
    }
    for (auto const& w: words) std::cout << w << "\n";
}

auto strip(string s) -> string {
    auto result = string{};
    std::ranges::copy_if(s, std::back_inserter(result), [](auto c) { return std::isalpha(c); });
    return result;
}
