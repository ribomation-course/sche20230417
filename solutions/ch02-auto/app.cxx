#include <iostream>
#include <map>

using std::cout;

auto fib(unsigned n) {
    if (n == 0) return 0UL;
    if (n == 1) return 1UL;
    return fib(n - 2) + fib(n - 1);
}

struct Pair {
    unsigned arg;
    unsigned long res;
};

auto compute(unsigned n) {
    return Pair{n, fib(n)};
}

auto table(unsigned n) {
    auto tbl = std::map<unsigned, unsigned long>{};
    for (auto k = 1U; k <= n; ++k) {
        auto [a, f] = compute(k);
        tbl[a] = f;
    }
    return tbl;
}

int main() {
    auto const N = 10;
    {
        auto result = fib(N);
        cout << "fib(" << N << ") = " << result << "\n";
    }
    {
        auto [n, f] = compute(N);
        cout << "fib(" << n << ") = " << f << "\n";
    }
    cout<<"----\n";
    {
        for (auto [n, f]: table(N))
            cout << "fib(" << n << ") = " << f << "\n";
    }
}
