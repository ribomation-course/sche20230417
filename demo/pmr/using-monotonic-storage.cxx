#include <iostream>
#include <vector>
#include <memory_resource>

using std::begin;
using std::end;
using std::data;
using std::size;
using std::uninitialized_fill;

using std::pmr::monotonic_buffer_resource;
using std::pmr::null_memory_resource;
using std::pmr::vector;

using std::cout;
using std::endl;

int main() {
    auto const N = 64U;
    char storage[N];
    uninitialized_fill(begin(storage), end(storage), '.');
    auto memory = monotonic_buffer_resource{data(storage), size(storage), null_memory_resource()};

    auto chars = vector<char>{&memory};
    for (auto ch = 'a'; ch <= 'z'; ++ch) chars.push_back(ch);
    storage[N-1] = '\0';

    cout << "#storage [" << storage << "]\n";

    return 0;
}
