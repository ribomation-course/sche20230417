cmake_minimum_required(VERSION 3.18)
project(classic_vs_modern_cxx LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(classic-cxx classic-cxx.cxx)
#target_compile_options(classic-cxx PRIVATE ${WARN})

add_executable(modern-cxx modern-cxx.cxx)
target_compile_options(modern-cxx PRIVATE ${WARN})

