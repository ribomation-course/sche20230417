#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>

struct Account {
    char* accno;
    int balance;
    Account(const char* a, int b) {
        accno = strdup(a);
        balance = b;
    }
    ~Account() { free(accno); }
};

char* to_upper(char* str) {
    int len = strlen(str);
    char* res = (char*) (malloc(len + 1));
    for (char* p = str, * q = res; *p; ++p, ++q) *q = toupper(*p);
    return res;
}

Account** create(int size) {
    Account** accounts = new Account* [size];
    for (int k = 0; k < size; ++k) {
        char buf[64];
        sprintf(buf, "bank-%04d", (k * 1234) % 10000);
        accounts[k] = new Account(to_upper(buf), k * 25);
    }
    return accounts;
}

void print(Account** arr, int size) {
    for (int k = 0; k < size; ++k)
        printf("Account{%s, %d}\n", arr[k]->accno, arr[k]->balance);
}

void update(Account** arr, int size) {
    for (int k = 0; k < size; ++k) arr[k]->balance *= 10;
}

void dispose(Account** arr, int size) {
    for (int k = 0; k < size; ++k) delete arr[k];
}

void handle(Account a) {
    printf("Account{%s, %d}\n", a.accno, a.balance);
}

int main(int argc, char** argv) {
    int N = argc == 1 ? 5 : atoi(argv[1]);
    Account** accounts = create(N);
    print(accounts, N);

    printf("----\n");
    update(accounts, N);
    print(accounts, N);
    dispose(accounts, N);

    printf("----\n");
    Account* a = new Account("QWERTY", 100);
    handle(*a);
    printf("Account{%s, %d}\n", a->accno, a->balance *= -1);
    if (a->balance < 0) return 0;
    delete a;
}

