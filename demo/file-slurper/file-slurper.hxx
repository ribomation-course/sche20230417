#pragma once
#include <string>
#include <fstream>
#include <stdexcept>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    class FileSlurper {
        ifstream file;

    public:
        FileSlurper(const string& filename) : file{filename} {
            if (!file) throw invalid_argument("cannot open "s + filename);
        }

        class iterator {
            ifstream* file = nullptr;
            bool   eof = false;
            string currentLine;

            string readLine() {
                string line;
                getline(*file, line);
                eof = !file->good();
                return line;
            }

        public:
            iterator() : eof{true} {}
            iterator(ifstream* file) : file{file} { currentLine = readLine(); }

            void    operator ++() { currentLine = readLine(); }
            string  operator *() const { return currentLine; }
            bool    operator !=(const iterator& that) const {
                return (*this).eof != that.eof;
            }
        };

        iterator begin() { return {&file}; }
        iterator end()   { return {}; }
    };
}
