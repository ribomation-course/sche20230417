#include <iostream>
#include <thread>
#include <chrono>
using std::cout;
using namespace std::chrono_literals;

int main() {
    cout << "[main] enter\n";
    {
        auto stopSrc = std::stop_source{};
        auto thr = std::jthread{[stop = stopSrc.get_token()]() {
            cout << "[thread] started\n";
            while (true) {
                cout << "." << std::flush;
                std::this_thread::sleep_for(250ms);
                if (stop.stop_requested()) {
                    cout << "\n[thread] terminating\n";
                    return;
                }
            }
        }};
        cout << "[main] sleeping\n";
        std::this_thread::sleep_for(2s);
        cout << "[main] stopping\n";
        stopSrc.request_stop();
    }
    cout << "[main] exit\n";
}
