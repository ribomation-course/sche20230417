#include <iostream>
#include <iomanip>
#include <sstream>
#include <thread>
using namespace std;
using namespace std::literals;
using namespace std::chrono;
using namespace std::chrono_literals;

int main() {
    auto body = [](auto name, auto duration) {
        do {
            auto          ts = system_clock::to_time_t(system_clock::now());
            ostringstream buf;
            buf << name << ": " << put_time(localtime(&ts), "%T") << "\n";
            cout << buf.str() << flush;

            this_thread::sleep_for(duration);
        } while (true);
    };

    thread t1(body, "T1"s, 3s);
    thread t2(body, "    T2"s, 750ms);
    thread t3(body, "        T3"s, 0.1min);

    t1.join(); t2.join(); t3.join();
    return 0;
}

