cmake_minimum_required(VERSION 3.22)
project(support_types)

set(CMAKE_CXX_STANDARD 23)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(string string.cxx)
target_compile_options(string PRIVATE ${WARN})

add_executable(string-view string-view.cxx)
target_compile_options(string-view PRIVATE ${WARN})

add_executable(tuple tuple.cxx)
target_compile_options(tuple PRIVATE ${WARN})

add_executable(optional optional.cxx)
target_compile_options(optional PRIVATE ${WARN})

add_executable(variant variant.cxx)
target_compile_options(variant PRIVATE ${WARN})

add_executable(visitor visitor.cxx)
target_compile_options(visitor PRIVATE ${WARN})


