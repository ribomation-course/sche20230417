#include <iostream>
#include <string>
#include <optional>
#include <vector>

using namespace std::string_literals;
using std::cout;
using std::string;

struct Contact {
    const string name{};
    std::optional<string> email{};
    std::optional<string> company{};
    std::optional<string> address{};
};
auto operator <<(std::ostream& os, Contact const& c) -> std::ostream& {
    os << "Contact{" << c.name;
    if (c.email)   os << ", " << *c.email;
    if (c.company) os << ", " << *c.company;
    if (c.address) os << ", " << *c.address;
    return os << "}";
}

int main() {
    auto customers = std::vector<Contact>{
        {"Anna"s},
        {"Berit"s, "berit@bugsify.ltd"s},
        {"Carin"s, std::nullopt, "Faulty Soft Inc."s},
        {"Doris"s, std::nullopt, std::nullopt, "42 Hacker Lane"s},
        {"Eva"s, "eva@bugsify.ltd"s, "Bugsify Ltd."s, "42 Hacker Lane"s},
    };
    for (auto const& c : customers) cout << c << "\n";
}

