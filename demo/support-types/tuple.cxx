#include <iostream>
#include <string_view>
#include <tuple>
using namespace std::string_view_literals;
using std::cout;
using std::string_view;

auto parse(string_view payload) -> std::tuple<string_view, string_view, string_view> {
    auto const SPACE = " "sv;
    auto const CRNL  = "\r\n"sv;
    auto space1 = payload.find(SPACE, 0);
    auto space2 = payload.find(SPACE, space1 + 1);
    auto end    = payload.find(CRNL, space2 + 1);
    return {
            payload.substr(0, space1),
            payload.substr(space1 + 1, space2 - space1 - 1),
            payload.substr(space2 + 1, end - space2 - 1)
    };
}

int main() {
    char data[] = "POST /api/customer/42 HTTP/1.1\r\nsome other stuff...";
    auto [operation, uri, protocol] = parse(std::string_view{data});
    cout << "HTTP: [" << operation << "] [" << uri << "] [" << protocol << "]\n";
}

